Person class
	- name
	- Library x 3 - Books, Albums, Movies
	- controller to Add, Delete, List Persons
	- Add, Delete, List ILibraryItems
	- test for Duplicate when adding
	- if a List is empty, warn user / allow to add items
	- save / load lists 

Book, Album, Movie classes
	- implement ILibraryItem interface
	- library item type - enum
	- Add, Delete, List items
	- interface has Title, Author, ReleaseDate
	- author: book, writer ; movie, director ; album, artist
	- add'l field: book, publisher ; movie, studio ; album, label


"On exit of the program prompt user to save changes.
Items should be saved to a JSON file in a Data sub-directory of the application's working directory.
The same file should be loaded on launch of the application."



release date - YYYY-MM-DD

alphabetise people?


//testing

Book hpAzk = new Book("Harry Potter and the Prisoner of Azkaban", "J.K. Rowling", new DateTime(1999,7,8), "Bloomsbury");
Console.WriteLine(hpAzk);

PeopleController myPeopleController = new PeopleController();
myPeopleController.AddPerson("April");
Console.WriteLine(myPeopleController.GetPerson("April"));



//MainController.cs
public bool ValidInput(string input, InputType inputType)
{
    switch (inputType)
    {
        case InputType.Navigation:

            break;
        case InputType.Text:

            break;
    }
}

public enum InputType { Navigation, Text }




//Save stuff
public void SaveInput()
{
    string result = Console.ReadLine().ToLower();
    if (result.Equals("y") || result.Equals("yes"))
    {

        Save();
                

        Console.WriteLine();
    }
    else if(result.Equals("n") || result.Equals("no")) return;
    else
        Console.WriteLine("Sorry, didn't quite catch that.");
        SaveInput();
        return;
    }

public void Save()
{
    string currentPath = Directory.GetCurrentDirectory();
    if (!Directory.Exists(Path.Combine(currentPath, "Data")))
    {
        Directory.CreateDirectory(Path.Combine(currentPath, "Data"));
    }

    string filePath = Path.Combine(currentPath, @"Data\test.txt");

    if (File.Exists(filePath))
    {
        File.Delete(filePath);
    }
            
    using (FileStream fileStream = File.Create(filePath))
    {
        Byte[] sampleText = new UTF8Encoding(true).GetBytes("Testing");
        fileStream.Write(sampleText, 0, sampleText.Length);
    }
            
    using (StreamReader streamReader = File.OpenText(filePath))
    {
        string s;
        while ((s = streamReader.ReadLine()) != null)
        {
            Console.WriteLine(s);
        }
    }
}

public StreamReader LibraryJson()
{

}


// Get item by type, in the idea that you could have multiple items with the same name if they are of different types
public ILibraryItem GetItem(Person libraryOwner, ItemType type, string title)
{
    List<ILibraryItem> itemsWithType = new List<ILibraryItem>();
    foreach (ILibraryItem i in libraryOwner.Items)
    {
        if (i.Type == type)
        {
            itemsWithType.Add(i);
        }
    }

    if (itemsWithType.Count == 0)
    {
        return null;
    }
    else
    {
        return itemsWithType.Find(x => x.Title.Contains(title));
    }
}