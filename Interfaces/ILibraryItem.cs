﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalLibrary
{
    public enum ItemType { Book, Album, Movie }
    public interface ILibraryItem
    {
        string Title { get; set; }
        string Author { get; set; }
        DateTime ReleaseDate { get; set; }
        ItemType Type { get; }
    }
}
