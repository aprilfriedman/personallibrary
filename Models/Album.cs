﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalLibrary.Models
{
    class Album : ILibraryItem
    {
        public Album(string title, string author, DateTime releaseDate, string label)
        {
            Title = title;
            Author = author;
            ReleaseDate = releaseDate;
            Label = label;
        }

        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Label { get; set; }
        public ItemType Type { get; } = ItemType.Album;
        public override string ToString() => $"{Author} - {Title} ({Label}, {ReleaseDate.ToString("yyyy-MM-dd")})";
    }
}
