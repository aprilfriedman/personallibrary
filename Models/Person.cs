﻿using System;
using System.Collections.Generic;
using PersonalLibrary.Models;

namespace PersonalLibrary
{
    class Person
    {
        public Person(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        public List<ILibraryItem> Items { get; } = new List<ILibraryItem>();

        public override string ToString() => Name;
    }
}