﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalLibrary.Models
{
    class Book : ILibraryItem
    {
        public Book(string title, string author, DateTime releaseDate, string publisher)
        {
            Title = title;
            Author = author;
            ReleaseDate = releaseDate;
            Publisher = publisher;
        }

        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Publisher { get; set; }
        public ItemType Type { get; } = ItemType.Book;
        public override string ToString() => $"{Author} - {Title} ({Publisher}, {ReleaseDate.ToString("yyyy-MM-dd")})";
    }
}
