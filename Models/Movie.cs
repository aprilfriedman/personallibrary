﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalLibrary.Models
{
    class Movie : ILibraryItem
    {
        public Movie(string title, string author, DateTime releaseDate, string studio)
        {
            Title = title;
            Author = author;
            ReleaseDate = releaseDate;
            Studio = studio;
        }

        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Studio { get; set; }
        public ItemType Type { get; } = ItemType.Movie;
        public override string ToString() => $"{Author} - {Title} ({Studio}, {ReleaseDate.ToString("yyyy-MM-dd")})";
    }
}
