﻿using System;
using PersonalLibrary.Models;
using PersonalLibrary.Controllers;

namespace PersonalLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
            MainController controller = new MainController();
            controller.Start();
        }
    }
}