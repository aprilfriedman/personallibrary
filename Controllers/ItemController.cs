﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PersonalLibrary.Models;

namespace PersonalLibrary.Controllers
{
    class ItemController
    {
        private MainController mainCtrlr;

        public ItemController (MainController main)
        {
            mainCtrlr = main;
        }

        public void AddItem(Person libraryOwner, ItemType type, string title, string author, DateTime releaseDate, string otherField)
        {
            switch (type)
            { // Add the item to the list as the specific class of what type it is
                case ItemType.Book:
                    libraryOwner.Items.Add(new Book(title, author, releaseDate, otherField));
                    break;

                case ItemType.Album:
                    libraryOwner.Items.Add(new Album(title, author, releaseDate, otherField));
                    break;

                case ItemType.Movie:
                    libraryOwner.Items.Add(new Movie(title, author, releaseDate, otherField));
                    break;

                default:
                    return;
            }
        }

        public void RemoveItem(Person libraryOwner, string title)
        {
            libraryOwner.Items.Remove(GetItem(libraryOwner, title));
        }

        public ILibraryItem GetItem(Person libraryOwner, string title)
        {
            return libraryOwner.Items.Find(x => x.Title.Contains(title));
        }
    }
}