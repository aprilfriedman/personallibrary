﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using PersonalLibrary.Models;

namespace PersonalLibrary.Controllers
{
    class MainController
    {
        public PeopleController PeopleCtrlr { get; }
        public LibraryController LibraryCtrlr { get; }

        public MainController()
        {
            PeopleCtrlr = new PeopleController(this);
            LibraryCtrlr = new LibraryController(this);
        }
        
        public void Start()
        {
            Header();
            Menu();
        }

        public void Header()
        {
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("|         Welcome to the          |");
            Console.WriteLine("| P E R S O N A L   L I B R A R Y |");
            Console.WriteLine("-----------------------------------");
        }

        public void Menu()
        {
            Console.WriteLine("\nWhat would you like to do?");
            Console.WriteLine("\n1: Edit People\n2: Edit Library\n3: Exit\n");
            string result = Console.ReadLine();

            switch (result)
            {
                case "1": // Edit People
                    PeopleCtrlr.Start();
                    break;

                case "2": // Edit Library
                    LibraryCtrlr.Start();
                    break;
                    
                case "3": // Exit
                    Exit();
                    break;

                case "help":
                    Console.WriteLine("You can't be helped.");
                    Menu();
                    break;

                default: // Allow user to try again
                    Console.WriteLine("Sorry, didn't quite catch that.");
                    Menu();
                    break;
            }
        }


        public void Exit()
        {
            Console.WriteLine("\nAre you sure you would like to exit? (Y/N)");
            string result = Console.ReadLine().ToLower();

            if ( result.Equals("y") || result.Equals("yes"))
            {
                Console.WriteLine("\nOkay! Thanks for using the Personal Library!");
                Environment.Exit(0);
            }
            else if (result.Equals("n") || result.Equals("no"))
            {
                Menu();
                return;
            }
            else
            {
                Console.WriteLine("Sorry, didn't quite catch that.");
                Exit();
                return;
            }
            
        }
    }
}