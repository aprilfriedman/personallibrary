﻿using System;
using System.Collections.Generic;
using PersonalLibrary.Models;

namespace PersonalLibrary.Controllers
{
    class LibraryController
    {
        private static MainController mainCtrlr;
        public Person LibraryOwner { get; set; }
        private ItemController itemCtrlr = new ItemController(mainCtrlr);

        public LibraryController(MainController main)
        {
            mainCtrlr = main;
        }

        public void Start()
        {
            // Prompt user to choose which Person's library they would like to edit
            // The ListPeopleOutput method includes a prompt to add a Person if there are none, so the user can't enter the menu without assigning a Person
            mainCtrlr.PeopleCtrlr.ListPeopleOutput();

            if (mainCtrlr.PeopleCtrlr.People.Count > 0)
            {
                Console.WriteLine("\nWhose library would you like to edit?");

                SetPerson();

                Menu();
            }
            else
            {
                mainCtrlr.Menu();
                return;
            }
        }

        public void Menu()
        {
            Console.WriteLine($"\nWhat would you like to do with {LibraryOwner.Name}'s library?");
            Console.WriteLine("\n1: Modify a Section\n2: Clear Library\n3: View Library\n4: Return to Main Menu\n");
            MenuInput();
        }

        public void MenuInput()
        {
            string result = Console.ReadLine();

            switch (result)
            {
                case "1": // Modify a Section
                    SectionMenu();
                    break;

                case "2": // Clear Library
                    Console.WriteLine($"\nAre you sure you would like to clear all items from {LibraryOwner.Name}'s library? This cannot be undone. (Y/N)");

                    string response = Console.ReadLine().ToLower();
                    if (response.Equals("y") || response.Equals("yes"))
                    {
                        ClearLibrary();
                        Console.WriteLine($"\nAll items in {LibraryOwner.Name}'s library have been removed.");
                        Menu();
                    }
                    else
                    {
                        Menu();
                    }
                    break;

                case "3": // View Library
                    ViewLibrary();
                    Menu();
                    break;

                case "4": // Return to Main Menu
                    mainCtrlr.Menu();
                    break;

                default:
                    Console.WriteLine("\nSorry, didn't quite catch that.");
                    MenuInput();
                    break;
            }
        }

        public void SectionMenu()
        {
            Console.WriteLine($"\nWhich section of {LibraryOwner.Name}'s library would you like to modify?");
            Console.WriteLine("\n1: Books\n2: Albums\n3: Movies\n4: Return to Library Menu\n");
            SectionMenuInput();
        }

        public void SectionMenuInput()
        {
            string result = Console.ReadLine();

            switch (result)
            {
                case "1": // Books
                    ModifyItemMenu(ItemType.Book);
                    break;
                case "2": // Albums
                    ModifyItemMenu(ItemType.Album);
                    break;
                case "3": // Movies
                    ModifyItemMenu(ItemType.Movie);
                    break;
                case "4": // Library Menu
                    Menu();
                    break;
                default:
                    Console.WriteLine("\nSorry, didn't quite catch that.");
                    SectionMenuInput();
                    break;
            }
        }

        public void ModifyItemMenu(ItemType type)
        {
            Console.WriteLine($"\nWhat would you like to do?");
            Console.WriteLine($"\n1: Add {type.ToString()}\n2: Remove {type.ToString()}\n3: List {type.ToString()}s\n4: Return to Section Menu\n");
            ModifyItemMenuInput(type);
        }

        public void ModifyItemMenuInput(ItemType type)
        {
            string result = Console.ReadLine();

            switch (result)
            {
                case "1": // Add Item
                    Console.WriteLine($"\nWhat is the name of the {type.ToString().ToLower()} you would like to add?");
                    AddItemInput(type);
                    ModifyItemMenu(type);
                    break;
                case "2": // Remove Item
                    if (ItemsWithType(type).Count > 0)
                    {
                        ViewLibrary(type);
                        Console.WriteLine($"\nWhat is the name of the {type.ToString().ToLower()} you would like to remove?");
                        RemoveItemInput(type);
                    }
                    else // If there is no items of this type, fail
                    {
                        Console.WriteLine($"\nSorry, {LibraryOwner.Name} has no {type.ToString().ToLower()}s in their library.");
                    }
                    ModifyItemMenu(type);
                    break;
                case "3": // List Items
                    ViewLibrary(type);
                    ModifyItemMenu(type);
                    break;
                case "4": // Section Menu
                    SectionMenu();
                    break;
                default:
                    Console.WriteLine("\nSorry, didn't quite catch that.");
                    SectionMenuInput();
                    break;
            }
        }

        public void AddItemInput(ItemType type)
        {
            string title = Console.ReadLine();
            if (String.IsNullOrEmpty(title))
            {
                Console.WriteLine("\nSorry, didn't quite catch that.");
                AddItemInput(type);
                return;
            }
            else if (itemCtrlr.GetItem(LibraryOwner, title) != null)
            {
                Console.WriteLine("\nSorry, that name is already in use. Please enter another name.");
                AddItemInput(type);
                return;
            }
            else
            {
                string authorType;
                string otherField;
                switch (type)
                {
                    case ItemType.Album: 
                        authorType = "Artist";
                        otherField = "Label";
                        break;

                    case ItemType.Movie: 
                        authorType = "Director";
                        otherField = "Studio";
                        break;

                    default: // Book (and general fallback just in case)
                        authorType = "Author";
                        otherField = "Publisher";
                        break;
                }

                // Prompts for additional fields of the item

                // Author
                Console.WriteLine($"\nWho is the {authorType.ToLower()} of this {type.ToString().ToLower()}?");
                string author = FieldInput();

                // ReleaseDate
                Console.WriteLine($"\nWhat day was this {type.ToString().ToLower()} released? (YYYY-MM-DD)");
                DateTime releaseDate = ReleaseDateInput();

                // Other field (here Publisher/Label/Studio)
                Console.WriteLine($"\nWhat is the {otherField.ToLower()} of this {type.ToString().ToLower()}?");
                string other = FieldInput();

                itemCtrlr.AddItem(LibraryOwner, type, title, author, releaseDate, other);
                // (could imagine generalising using params on the constructor for a variable number of fields)

                Console.WriteLine($"\nWonderful! {title} has been added to {LibraryOwner.Name}'s library.");
                return;
            }
        }

        public string FieldInput()
        {
            string response = Console.ReadLine();

            if (String.IsNullOrEmpty(response))
            {
                Console.WriteLine("\nSorry, didn't quite catch that.");
                return FieldInput();
            }
            else
            {
                return response;
            }
        }

        public DateTime ReleaseDateInput()
        {
            DateTime releaseDate;
            string response = Console.ReadLine();
            
            string[] rdSplit = response.Split('-');
            if (response.Length != 10 // If too short in characters
                || rdSplit.Length != 3 // If doesn't split into three parts with '-'
                || !int.TryParse(rdSplit[0], out int tempInt) // If any of the three parts are not valid integers
                || !int.TryParse(rdSplit[1], out tempInt)
                || !int.TryParse(rdSplit[2], out tempInt))
            {
                Console.WriteLine("\nSorry, didn't quite catch that.");
                return ReleaseDateInput();
            }
            else // Otherwise, parse as DateTime
            {
                releaseDate = new DateTime(int.Parse(rdSplit[0]), int.Parse(rdSplit[1]), int.Parse(rdSplit[2]));
                return releaseDate;
            }
        }

        public void RemoveItemInput(ItemType type)
        {
            string title = Console.ReadLine();
            if (String.IsNullOrEmpty(title))
            {
                Console.WriteLine("\nSorry, didn't quite catch that.");
                RemoveItemInput(type);
                return;
            }
            else if (itemCtrlr.GetItem(LibraryOwner, title) == null) // If item doesn't appear in library, fail
            {
                Console.WriteLine($"\nSorry, there is no {type.ToString().ToLower()} with that title in {LibraryOwner.Name}'s library.");
                return;
            }
            else
            {
                Console.WriteLine($"\nAre you sure you would like to remove {title}? (Y/N)");

                string result = Console.ReadLine().ToLower();
                if (result.Equals("y") || result.Equals("yes"))
                {
                    itemCtrlr.RemoveItem(LibraryOwner, title);
                    Console.WriteLine($"\n{title} has been removed from {LibraryOwner.Name}'s library.");
                    return;
                }
                else
                {
                    return;
                }
            }
            
        }

        public void ClearLibrary()
        {
            if (LibraryOwner.Items.Count > 0)
            {
                foreach (ILibraryItem i in LibraryOwner.Items.ToArray()) // Remove each item in library, using ToArray() so that it doesn't try to iterate through the changing list
                {
                    itemCtrlr.RemoveItem(LibraryOwner, i.Title);
                }
            }
        }

        public void ViewLibrary()
        {
            if(LibraryOwner.Items.Count == 0)
            {
                Console.WriteLine($"\n{LibraryOwner.Name}'s library is empty.");
            }
            else {
                ViewLibrary(ItemType.Book);
                ViewLibrary(ItemType.Album);
                ViewLibrary(ItemType.Movie);
            }
        }

        public void ViewLibrary(ItemType type) // Effectively a section viewer, though used with each type to create full library view
        {
            List<ILibraryItem> itemsWithType = ItemsWithType(type);

            if (itemsWithType.Count == 0)
            {
                Console.WriteLine($"\n{LibraryOwner.Name} has no {type.ToString().ToLower()}s in their library.");
            }
            else
            {
                Console.WriteLine($"\n{LibraryOwner.Name}'s {type.ToString()}s:");
                foreach (ILibraryItem i in itemsWithType)
                {
                    Console.WriteLine(i);
                }
            }
        }

        public List<ILibraryItem> ItemsWithType(ItemType type)
        {
            List<ILibraryItem> itemsWithType = new List<ILibraryItem>();
            foreach (ILibraryItem i in LibraryOwner.Items) // Grab only the items in the list with the specified type
            {
                if (i.Type == type)
                {
                    itemsWithType.Add(i);
                }
            }
            return itemsWithType;
        }

        public void SetPerson()
        {
            string name = Console.ReadLine();

            if (mainCtrlr.PeopleCtrlr.GetPerson(name) == null)
            {
                Console.WriteLine("\nSorry, that is not a member of the Personal Library.");
                SetPerson();
                return;
            }
            else
            {
                LibraryOwner = mainCtrlr.PeopleCtrlr.GetPerson(name);
            }
        }
    }
}