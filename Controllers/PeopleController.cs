﻿using System;
using System.Collections.Generic;
using PersonalLibrary.Models;

namespace PersonalLibrary.Controllers
{
    class PeopleController
    {
        public List<Person> People { get; } = new List<Person>();

        private MainController mainCtrlr;

        public PeopleController(MainController main)
        {
            mainCtrlr = main;
        }

        public void Start()
        {
            Menu();
        }

        public void Menu()
        {
            Console.WriteLine("\nWhat would you like to modify in the people list?");
            Console.WriteLine("\n1: Add a Person\n2: Remove a Person\n3: List All People\n4: Return to Main Menu\n");
            MenuInput();
        }

        public void MenuInput()
        {
            string result = Console.ReadLine();

            switch (result)
            {
                case "1": // Add a Person
                    Console.WriteLine("\nWhat is the name of the person you would like to add?");
                    AddPersonInput();
                    Menu();
                    break;

                case "2": // Remove a Person
                    if (People.Count > 0)
                    {
                        ListPeople();
                        Console.WriteLine("\nWho would you like to remove?");
                        RemovePersonInput();
                    }
                    else // If there is no Person in People, fail
                    {
                        Console.WriteLine("Sorry, there are no people in the Personal Library yet for you to remove.");
                    }
                    Menu();
                    break;

                case "3": // List All People
                    ListPeopleOutput();
                    Menu();
                    break;

                case "4": // Return to Main Menu
                    mainCtrlr.Menu();
                    break;

                default:
                    Console.WriteLine("\nSorry, didn't quite catch that.");
                    MenuInput();
                    break;
            }
        }

        public void AddPersonInput()
        {
            string name = Console.ReadLine();
            if (String.IsNullOrEmpty(name)) // If the user didn't enter anything
            {
                Console.WriteLine("\nSorry, didn't quite catch that.");
                AddPersonInput();
                return;
            }
            else if(GetPerson(name) != null) // If the Person already exists in People
            {
                Console.WriteLine("\nSorry, that name is already in use. Please enter another name.");
                AddPersonInput();
                return;
            }
            else
            {
                AddPerson(name);
                Console.WriteLine($"\nPerfect! {name} is now a member of the Personal Library.");
                return;
            }
        }

        public void RemovePersonInput()
        {
            string name = Console.ReadLine();
            if (String.IsNullOrEmpty(name))
            {
                Console.WriteLine("\nSorry, didn't quite catch that.");
                RemovePersonInput();
                return;
            }
            else if (GetPerson(name) == null) // If the Person doesn't exist in People
            {
                Console.WriteLine("\nSorry, that is not a member of the Personal Library.");
                return;
            }
            else
            {
                Console.WriteLine($"\nAre you sure you would like to remove {name}? (Y/N)");

                string result = Console.ReadLine().ToLower();
                if (result.Equals("y") || result.Equals("yes"))
                {
                    RemovePerson(name);
                    Console.WriteLine($"\n{name} has been removed from the Personal Library.");
                    return;

                }
                else
                {
                    return;
                }
            }
        }

        public void ListPeopleOutput()
        {
            if (People.Count > 0)
            {
                ListPeople();
                return;
            }
            else // If there are no members of People, prompt the user to add a Person
            {
                Console.WriteLine("\nThere are no users currently registered with the Personal Library. Would you like to add one? (Y/N)");

                string result = Console.ReadLine().ToLower();

                if (result.Equals("y") || result.Equals("yes"))
                {
                    Console.WriteLine("\nWhat is the name of the person you would like to add?");
                    AddPersonInput();
                    return;
                }
                else
                {
                    return;
                }
            }

        }
        
        public void AddPerson(string name)
        {
            People.Add(new Person(name));
        }

        public void RemovePerson(string name)
        {
            People.Remove(GetPerson(name));
        }

        public void ListPeople()
        {
            Console.WriteLine("\nHere are all of the people currently registered with the Personal Library:");
            foreach (Person i in People)
            {
                Console.WriteLine(i);
            }
            return;
        }


        public Person GetPerson(string name)
        {
            return People.Find(x => x.Name.Contains(name));
            // If Person does not exist in People, returns null
        }
    }
}